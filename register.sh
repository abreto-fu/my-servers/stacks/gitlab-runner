#!/usr/bin/env sh

set -x

docker run --rm -it -v $(pwd)/runner:/etc/gitlab-runner gitlab/gitlab-runner register
