# gitlab runner

Deploy a GitLab Runner to our hosts.

## How to register a new runner

Somewhere, run

```sh
$ git clone <this repo>.git gitlab-runner
$ cd gitlab-runner
$ ./register.sh
```

Follow the instructions, and then commit changes and push it to the repository.

CI would do boring works for you.
